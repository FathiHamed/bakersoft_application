from django.urls import path

from time_tracker.views import ProjectList, ProjectCreate, ProjectRetrieveUpdate, ProjectDelete, TimeLogCreate, \
    TimeLogList, TimeLogRetrieveUpdate, TimeLogDelete

urlpatterns = [
    path('project/list/', ProjectList.as_view(), name='project-list'),
    path('time_log/list/', TimeLogList.as_view(), name='time-log-list'),
    path('project/create/', ProjectCreate.as_view(), name='project-create'),
    path('time_log/create/', TimeLogCreate.as_view(), name='time-log-create'),
    path('project/<int:pk>/', ProjectRetrieveUpdate.as_view(), name='project-retrieve-update'),
    path('time_log/<int:pk>/', TimeLogRetrieveUpdate.as_view(), name='time-log-retrieve-update'),
    path('project/<int:pk>/delete/', ProjectDelete.as_view(), name='project-delete'),
    path('time_log/<int:pk>/delete/', TimeLogDelete.as_view(), name='time-log-delete'),
]
