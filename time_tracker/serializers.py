from collections import OrderedDict

from django.contrib.auth.models import User
from django.db.models import Q
from rest_framework import serializers

from time_tracker.models import Project, TimeLog


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = '__all__'


class TimeLogSerializer(serializers.ModelSerializer):
    username = serializers.CharField(required=False)
    project_name = serializers.CharField(required=False)

    class Meta:
        model = TimeLog
        exclude = ('user', 'project')

    def create(self, validated_data):
        username = validated_data.pop('username')
        project_name = validated_data.pop('project_name')
        start_time = validated_data.get('start_time')
        end_time = validated_data.get('end_time')
        user = User.objects.get(username=username)
        project = Project.objects.get(name=project_name)
        self.validate_new_time_log(user, project, start_time, end_time)
        time_log = TimeLog.objects.create(user=user, project=project, **validated_data)
        return time_log

    def update(self, instance, validated_data):
        user = instance.user
        project = instance.project
        start_time = instance.start_time
        end_time = instance.end_time
        if 'username' in validated_data.keys():
            username = validated_data.pop('username')
            user = User.objects.get(username=username)
        if 'project_name' in validated_data.keys():
            project_name = validated_data.pop('project_name')
            project = Project.objects.get(name=project_name)
        if 'start_time' in validated_data.keys():
            start_time = validated_data.get('start_time')
        if 'end_time' in validated_data.keys():
            end_time = validated_data.get('end_time')
        self.validate_update_time_log(user, project, start_time, end_time)
        instance = super(TimeLogSerializer, self).update(instance, validated_data)
        instance.user = user
        instance.project = project
        instance.save()
        return instance

    def to_representation(self, instance):
        username = instance.user.username
        project_name = instance.project.name
        instance = super(TimeLogSerializer, self).to_representation(instance)
        data = dict(instance)
        data.update({
            "username": username,
            "project_name": project_name
        })
        instance = OrderedDict(data)
        return instance

    def validate_new_time_log(self, user, project, start_time, end_time):
        queryset = TimeLog.objects.filter(user=user, end_time__isnull=True)
        if queryset.exists():
            raise serializers.ValidationError("An unfinished time log is already left!")
        self.validate_update_time_log(user, project, start_time, end_time)

    @staticmethod
    def validate_update_time_log(user, project, start_time, end_time):
        queryset = TimeLog.objects.filter(user=user, project=project, start_time=start_time, end_time=end_time)
        if queryset.exists():
            raise serializers.ValidationError("This time log is duplicate!")

        if end_time:
            if end_time <= start_time:
                raise serializers.ValidationError("Value of end_time should be greater than start_time!")
            queryset = TimeLog.objects.filter(
                user=user, project=project
            ).filter(
                Q(start_time__lte=start_time, end_time__gt=start_time) |
                Q(start_time__lt=end_time, end_time__gte=end_time) |
                Q(start_time__gte=start_time, end_time__lte=end_time)
            )
        else:
            queryset = TimeLog.objects.filter(
                user=user, project=project, start_time__lte=start_time, end_time__gt=start_time
            )
        if queryset.exists():
            raise serializers.ValidationError("This period of time overlaps with another time log!")
