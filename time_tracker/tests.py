from datetime import timedelta

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from rest_framework import status
from rest_framework.authtoken.models import Token

from time_tracker.models import Project, TimeLog


class TimeTrackerTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        projects = [
            Project(name="project1", category="development", description="description1"),
            Project(name="project2", status=2, category="development", description="description2"),
            Project(name="project3", category="test", description="description3"),
        ]
        Project.objects.bulk_create(projects)
        cls.projects = Project.objects.all()
        cls.test_user = User.objects.create_user(username="user1", email="user1@bakersoft.de", password="password1")
        cls.second_user = User.objects.create_user(username="user2", email="user2@bakersoft.de", password="password2")
        cls.test_user_token = Token.objects.get_or_create(user=cls.test_user)[0].key
        cls.admin_user = User.objects.create_superuser(
            username="admin1", email="admin1@bakersoft.de", password="password_admin1")
        cls.admin_token = Token.objects.get_or_create(user=cls.admin_user)[0].key

        time_logs = [
            TimeLog(user=cls.test_user, project=cls.projects[0], description="Doing task1 ...",
                    start_time=timezone.now() - timedelta(days=17), end_time=timezone.now() - timedelta(days=15)),
            TimeLog(user=cls.test_user, project=cls.projects[0], description="Doing task2 ...",
                    start_time=timezone.now() - timedelta(days=14), end_time=timezone.now() - timedelta(days=12)),
            TimeLog(user=cls.test_user, project=cls.projects[0], description="Doing task3 ...",
                    start_time=timezone.now() - timedelta(days=11), end_time=timezone.now() - timedelta(days=8)),
            TimeLog(user=cls.test_user, project=cls.projects[1], description="Doing task1 ...",
                    start_time=timezone.now() - timedelta(days=7), end_time=timezone.now() - timedelta(days=5)),
            TimeLog(user=cls.test_user, project=cls.projects[1], description="Doing task2 ...",
                    start_time=timezone.now() - timedelta(days=4)),
            TimeLog(user=cls.test_user, project=cls.projects[2], description="Doing task1 ...",
                    start_time=timezone.now() - timedelta(days=6), end_time=timezone.now() - timedelta(days=3)),
            TimeLog(user=cls.second_user, project=cls.projects[2], description="Doing task1 ...",
                    start_time=timezone.now() - timedelta(days=5), end_time=timezone.now() - timedelta(days=1)),
            TimeLog(user=cls.second_user, project=cls.projects[2], description="Doing task2 ...",
                    start_time=timezone.now() - timedelta(days=1)),
        ]
        TimeLog.objects.bulk_create(time_logs)

    def setUp(self):
        self.client.defaults['HTTP_AUTHORIZATION'] = 'Token ' + self.test_user_token

    def test_project_list_api(self):
        first_project = Project.objects.get(name='project1')
        second_project = Project.objects.get(name='project2')
        third_project = Project.objects.get(name='project3')
        response = self.client.get(reverse('project-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = response.json()['results']
        self.assertEqual(results[0]['name'], first_project.name)
        self.assertEqual(results[2]['category'], third_project.category)
        self.assertEqual(response.json()['count'], Project.objects.all().count())

        query_params = {
            'related_to_me': 'true'
        }
        response = self.client.get(reverse('project-list'), query_params)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = response.json()['results']
        self.assertEqual(results[0]['name'], first_project.name)
        self.assertEqual(results[1]['category'], second_project.category)
        self.assertEqual(response.json()['count'],
                         len(set(TimeLog.objects.filter(user=self.test_user).values_list('project_id'))))

        query_params = {
            'category': 'development',
            'status': 1
        }
        response = self.client.get(reverse('project-list'), query_params)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['count'], Project.objects.filter(**query_params).count())

    def test_project_retrieve_api(self):
        third_project = Project.objects.get(name='project3')
        response = self.client.get(reverse('project-retrieve-update', args=(third_project.id,)))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['id'], third_project.id)
        self.assertEqual(response.json()['name'], third_project.name)
        self.assertEqual(response.json()['category'], third_project.category)

    def test_project_update_api(self):
        second_project = Project.objects.get(name='project2')
        new_status = 3
        update_data = {
            'status': new_status
        }
        response = self.client.patch(
            reverse('project-retrieve-update', args=(second_project.id,)), update_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        self.client.defaults['HTTP_AUTHORIZATION'] = 'Token ' + self.admin_token
        response = self.client.patch(
            reverse('project-retrieve-update', args=(second_project.id,)), update_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['id'], second_project.id)
        self.assertEqual(response.json()['name'], second_project.name)
        self.assertEqual(response.json()['status'], new_status)

    def test_project_delete_api(self):
        project_id = 1
        response = self.client.delete(reverse('project-delete', args=(project_id,)))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        self.client.defaults['HTTP_AUTHORIZATION'] = 'Token ' + self.admin_token
        response = self.client.delete(reverse('project-delete', args=(project_id,)))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_time_log_list_api(self):
        # test_user can see her time logs and should not be able to see another user time logs
        query_params = {
            'username': self.second_user.username
        }
        response = self.client.get(reverse('time-log-list'), query_params)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['count'], TimeLog.objects.filter(user=self.test_user).count())

        second_project = Project.objects.get(name='project2')
        query_params = {
            'project_name': second_project.name
        }
        self.client.defaults['HTTP_AUTHORIZATION'] = 'Token ' + self.test_user_token
        response = self.client.get(reverse('time-log-list'), query_params)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = response.json()['results']
        queryset = TimeLog.objects.filter(user=self.test_user, project=second_project)
        self.assertEqual(results[0]['description'], queryset[0].description)
        self.assertEqual(results[1]['username'], self.test_user.username)
        self.assertEqual(results[1]['start_time'], queryset[1].start_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ"))
        self.assertEqual(response.json()['count'], queryset.count())

        start_time = timezone.now() - timedelta(days=15)
        end_time = timezone.now() - timedelta(days=4)
        query_params = {
            'start_time': str(start_time.timestamp()),
            'end_time': str(end_time.timestamp())
        }
        response = self.client.get(reverse('time-log-list'), query_params)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['count'],
                         TimeLog.objects.filter(
                             user=self.test_user, start_time__gte=start_time, end_time__lte=end_time
                         ).count())

        # admin_user can see the time logs of all users
        self.client.defaults['HTTP_AUTHORIZATION'] = 'Token ' + self.admin_token
        response = self.client.get(reverse('time-log-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['count'], TimeLog.objects.all().count())
        query_params = {
            'username': self.second_user.username
        }
        response = self.client.get(reverse('time-log-list'), query_params)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['count'], TimeLog.objects.filter(user=self.second_user).count())

    def test_time_log_retrieve_api(self):
        # test_user can see only her time logs and should not be able to see another user time logs
        first_time_log = TimeLog.objects.filter(user=self.second_user).first()
        response = self.client.get(reverse('time-log-retrieve-update', args=(first_time_log.id,)))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        second_time_log = TimeLog.objects.filter(user=self.test_user).first()
        response = self.client.get(reverse('time-log-retrieve-update', args=(second_time_log.id,)))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['id'], second_time_log.id)
        self.assertEqual(response.json()['username'], second_time_log.user.username)
        self.assertEqual(response.json()['project_name'], second_time_log.project.name)

        # admin_user can see the time logs of all users
        self.client.defaults['HTTP_AUTHORIZATION'] = 'Token ' + self.admin_token
        response = self.client.get(reverse('time-log-retrieve-update', args=(first_time_log.id,)))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['id'], first_time_log.id)
        self.assertEqual(response.json()['username'], first_time_log.user.username)
        self.assertEqual(response.json()['project_name'], first_time_log.project.name)

    def test_time_log_update_api(self):
        second_project = Project.objects.get(name='project2')
        third_project = Project.objects.get(name='project3')
        # test_user can change only her time logs
        first_time_log = TimeLog.objects.filter(
            user=self.second_user, project=third_project, description="Doing task2 ..."
        ).first()
        end_time = timezone.now() - timedelta(days=5)
        update_data = {
            'end_time': end_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        }
        response = self.client.patch(
            reverse('time-log-retrieve-update', args=(first_time_log.id,)), update_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        second_time_log = TimeLog.objects.filter(
            user=self.test_user, project=second_project, description="Doing task2 ..."
        ).first()
        response = self.client.patch(
            reverse('time-log-retrieve-update', args=(second_time_log.id,)), update_data, content_type='application/json')
        # end_time is less than start_time
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        start_time = timezone.now() - timedelta(days=6)
        end_time = timezone.now() - timedelta(days=3)
        update_data = {
            'start_time': start_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
            'end_time': end_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        }
        response = self.client.patch(
            reverse('time-log-retrieve-update', args=(second_time_log.id,)), update_data, content_type='application/json')
        # this period of time overlaps with another time log
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        end_time = timezone.now() - timedelta(days=2)
        update_data = {
            'end_time': end_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        }
        response = self.client.patch(
            reverse('time-log-retrieve-update', args=(second_time_log.id,)), update_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['end_time'],
                         TimeLog.objects.get(id=second_time_log.id).end_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ"))

    def test_time_log_delete_api(self):
        # test_user can delete only her time logs
        first_time_log = TimeLog.objects.filter(user=self.second_user).first()
        response = self.client.delete(reverse('time-log-delete', args=(first_time_log.id,)))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        second_time_log = TimeLog.objects.filter(user=self.test_user).first()
        response = self.client.delete(reverse('time-log-delete', args=(second_time_log.id,)))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        self.client.defaults['HTTP_AUTHORIZATION'] = 'Token ' + self.admin_token
        response = self.client.delete(reverse('time-log-delete', args=(first_time_log.id,)))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_time_log_create_api(self):
        project = Project.objects.get(name="project1")
        start_time = timezone.now() - timedelta(days=12)
        end_time = timezone.now() - timedelta(days=9)
        data = {
            'username': self.second_user.username,
            'project_name': project.name,
            'description': "test ...",
            'start_time': start_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
            'end_time': end_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        }
        response = self.client.post(reverse('time-log-create'), data=data)
        # test_user can create new time logs only for herself
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        data = {
            'username': self.test_user.username,
            'project_name': project.name,
            'description': "test ...",
            'start_time': start_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
            'end_time': end_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        }
        response = self.client.post(reverse('time-log-create'), data=data)
        # this period of time overlaps with another time log
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        start_time = timezone.now() - timedelta(days=8)
        end_time = timezone.now() - timedelta(days=7)
        data = {
            'username': self.test_user.username,
            'project_name': project.name,
            'description': "test ...",
            'start_time': start_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
            'end_time': end_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        }
        response = self.client.post(reverse('time-log-create'), data=data)
        # test_user can not create new time logs while she has an unfinished work
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        time_log = TimeLog.objects.filter(user=self.test_user, end_time__isnull=True).first()
        end_time = timezone.now() - timedelta(days=2)
        update_data = {
            'end_time': end_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        }
        # test_user should update the unfinished time log
        response = self.client.patch(
            reverse('time-log-retrieve-update', args=(time_log.id,)), update_data,
            content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # test_user can create new time logs successfully
        response = self.client.post(reverse('time-log-create'), data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
