from datetime import datetime

import pytz
from rest_framework import generics, permissions, status
from rest_framework.response import Response

from time_tracker.serializers import ProjectSerializer, TimeLogSerializer
from time_tracker.models import Project, TimeLog


class ProjectList(generics.ListAPIView):
    """ We should use GET method for fetching all projects. """
    serializer_class = ProjectSerializer

    def get_queryset(self):
        """
        Filters queryset using `relate_to_me`, `name`, `category` and `status` query parameters in the URL.
        """
        queryset = Project.objects.all()
        related_to_me = self.request.query_params.get('related_to_me')
        if related_to_me == "true":
            user = self.request.user
            related_project_ids = set(TimeLog.objects.filter(user=user).values_list('project_id', flat=True))
            queryset = queryset.filter(
                id__in=related_project_ids
            )
        filters = {
            'name': self.request.query_params.get('name'),
            'category': self.request.query_params.get('category'),
            'status': self.request.query_params.get('status'),
        }
        arguments = {}
        for k, v in filters.items():
            if v:
                arguments[k] = v
        queryset = queryset.filter(**arguments).order_by('update_time')
        return queryset


class ProjectCreate(generics.CreateAPIView):
    """ We should use POST method for creating a project. """

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    permission_classes = [permissions.IsAdminUser]


class ProjectRetrieveUpdate(generics.RetrieveUpdateAPIView):
    """
    We should use GET method for retrieving, PUT method for updating and PATCH method to apply partial modifications.
    """

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer

    class ReadOnly(permissions.BasePermission):
        def has_permission(self, request, view):
            return request.method in permissions.SAFE_METHODS

    permission_classes = [permissions.IsAdminUser | ReadOnly]


class ProjectDelete(generics.DestroyAPIView):
    """ We should use DELETE method for deleting a project. """

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    permission_classes = [permissions.IsAdminUser]


class TimeLogList(generics.ListAPIView):
    """ We should use GET method for fetching all time logs. """
    serializer_class = TimeLogSerializer

    def get_queryset(self):
        """
        Filters time logs using `username`, `project_name`, `status`, `start_time` and `end_time` query parameters in the URL.
        Consider that `start_time` and `end_time` parameters should be in timestamp format.
        """
        if self.request.user.is_superuser:
            queryset = TimeLog.objects.all()
            filters = {
                'user__username': self.request.query_params.get('username'),
                'project__name': self.request.query_params.get('project_name'),
                'status': self.request.query_params.get('status'),
            }
        else:
            queryset = TimeLog.objects.filter(user=self.request.user)
            filters = {
                'project__name': self.request.query_params.get('project_name'),
                'status': self.request.query_params.get('status'),
            }
        start_time_utc = self.request.query_params.get('start_time')
        if start_time_utc:
            start_time = pytz.utc.localize(datetime.utcfromtimestamp(float(start_time_utc)))
            filters.update({'start_time__gte': start_time})
        end_time_utc = self.request.query_params.get('end_time')
        if end_time_utc:
            end_time = pytz.utc.localize(datetime.utcfromtimestamp(float(end_time_utc)))
            filters.update({'end_time__lte': end_time})
        arguments = {}
        for k, v in filters.items():
            if v:
                arguments[k] = v
        queryset = queryset.filter(**arguments).order_by('update_time')
        return queryset


class TimeLogCreate(generics.CreateAPIView):
    """ We should use POST method for creating a project. """

    queryset = TimeLog.objects.all()
    serializer_class = TimeLogSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        username = request.data.get('username')
        if self.request.user.is_superuser or self.request.user.username == username:
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        result = {
            "error_message": "You don’t have permission to create this object!"
        }
        return Response(result, status=status.HTTP_403_FORBIDDEN)


class TimeLogRetrieveUpdate(generics.RetrieveUpdateAPIView):
    """
    We should use GET method for retrieving, PUT method for updating and PATCH method to apply partial modifications.
    """

    queryset = TimeLog.objects.all()
    serializer_class = TimeLogSerializer

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        if self.request.user.is_superuser or self.request.user == instance.user:
            serializer = self.get_serializer(instance)
            return Response(serializer.data)
        result = {
            "error_message": "You don’t have permission to retrieve this object!"
        }
        return Response(result, status=status.HTTP_403_FORBIDDEN)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        if self.request.user.is_superuser or self.request.user == instance.user:
            serializer = self.get_serializer(instance, data=request.data, partial=partial)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)

            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}

            return Response(serializer.data)
        result = {
            "error_message": "You don’t have permission to update this object!"
        }
        return Response(result, status=status.HTTP_403_FORBIDDEN)


class TimeLogDelete(generics.DestroyAPIView):
    """ We should use DELETE method for deleting a time log. """

    queryset = TimeLog.objects.all()
    serializer_class = ProjectSerializer

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        if self.request.user.is_superuser or self.request.user == instance.user:
            return self.destroy(request, *args, **kwargs)
        result = {
            "error_message": "You don’t have permission to delete this object!"
        }
        return Response(result, status=status.HTTP_403_FORBIDDEN)
