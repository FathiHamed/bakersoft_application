from django.db import models
from django.contrib.auth.models import User


class Project(models.Model):
    STATUS = (
        (1, "todo"),
        (2, "running"),
        (3, "finished"),
        (4, "canceled"),
    )
    name = models.CharField(null=False, unique=True, max_length=200)
    category = models.CharField(max_length=200, default="general")
    status = models.IntegerField(choices=STATUS, default=1)
    description = models.TextField(blank=True, null=True)
    creation_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)
    deadline = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return "%s" % self.name


class TimeLog(models.Model):
    STATUS = (
        (1, "unverified"),
        (2, "verified"),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='time_logs')
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='time_logs')
    status = models.IntegerField(choices=STATUS, default=1, blank=False, null=False)
    description = models.TextField(blank=True)
    creation_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return "user: %s, project: %s" % (self.user.username, self.project.name)

