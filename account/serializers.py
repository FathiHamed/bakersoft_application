from django.contrib.auth.models import User
from rest_framework import serializers


class RegistrationSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name', 'password', 'is_staff']
        extra_kwargs = {
            "first_name": {"required": True},
            "last_name": {"required": True}
        }

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user
