from django.urls import path

from account import views

urlpatterns = [
    path('register/', views.register, name='register-user'),
    path('login/', views.login_user, name='login-user'),
    path('logout/', views.logout_user, name='logout-user'),
]
