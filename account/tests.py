from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token


class AuthenticationTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.test_user_params = {
            'username': 'user1',
            'email': 'user1@bakersoft.de',
            'first_name': 'user1_first_name',
            'last_name': 'user1_last_name',
            'password': 'password1',
            'is_staff': False
        }
        test_user = User.objects.create_user(**cls.test_user_params)
        cls.test_user_token = Token.objects.get_or_create(user=test_user)[0].key

    def test_register_api(self):
        client = Client()
        user_params = {
            'username': 'user2',
            'email': 'user2@bakersoft.de',
            'first_name': 'user2_first_name',
            'last_name': 'user2_last_name',
            'password': 'password2',
            'is_staff': False
        }
        self.assertEqual(User.objects.filter(username=user_params['username']).exists(), False)
        response = client.post(reverse('register-user'), data=user_params)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.json()['success'], True)
        self.assertEqual(response.json()['username'], user_params['username'])
        self.assertEqual(response.json()['email'], user_params['email'])
        password = user_params.pop('password')
        queryset = User.objects.filter(**user_params)
        self.assertEqual(queryset.exists(), True)
        self.assertEqual(queryset.count(), 1)
        self.assertEqual(check_password(password, queryset.first().password), True)

    def test_login_api(self):
        client = Client()
        credentials = {
            'username': 'user2',
            'password': 'password2'
        }
        response = client.post(reverse('login-user'), credentials, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json()['success'], False)
        self.assertEqual(response.json()['message'], "Credentials are invalid!")

        credentials = {
            'username': self.test_user_params['username'],
            'password': 'password2'
        }
        response = client.post(reverse('login-user'), credentials, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json()['success'], False)
        self.assertEqual(response.json()['message'], "Credentials are invalid!")

        credentials = {
            'username': self.test_user_params['username'],
            'password': self.test_user_params['password']
        }
        response = client.post(reverse('login-user'), credentials, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['success'], True)
        self.assertEqual(response.json()['message'], f"User logged in with token: {self.test_user_token}")

    def test_logout_api(self):
        client = Client()
        response = client.get(reverse('logout-user'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        client.defaults['HTTP_AUTHORIZATION'] = 'Token ' + self.test_user_token
        response = client.get(reverse('logout-user'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
