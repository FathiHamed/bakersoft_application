import json

from django.contrib.auth import login, logout
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_200_OK

from account.serializers import RegistrationSerializer


@api_view(["POST"])
@permission_classes([AllowAny])
def register(request):
    serializer = RegistrationSerializer(data=request.data)
    if serializer.is_valid():
        user = serializer.save()
        token = Token.objects.get_or_create(user=user)[0].key
        result = {
            "success": True,
            "email": user.email,
            "username": user.username,
            "token": token
        }
        status_code = HTTP_201_CREATED
    else:
        result = {
            "success": False
        }
        result.update(serializer.errors)
        status_code = HTTP_400_BAD_REQUEST
    return JsonResponse(result, status=status_code)


@api_view(["POST"])
@permission_classes([AllowAny])
def login_user(request):
    message = "Credentials are invalid!"
    success = False
    status_code = HTTP_400_BAD_REQUEST
    data = json.loads(request.body)
    username = data.get('username')
    password = data.get('password')
    if username and password:
        try:
            user = User.objects.get(username=username)
        except ObjectDoesNotExist:
            user = None
        if user:
            token = Token.objects.get_or_create(user=user)[0].key
            if check_password(password, user.password):
                if user.is_active:
                    login(request, user)
                    message = f"User logged in with token: {token}"
                    success = True
                    status_code = HTTP_200_OK
                else:
                    message = "Account is not active!"

    result = {
        "success": success,
        "message": message
    }
    return JsonResponse(result, status=status_code)


@api_view(["GET"])
def logout_user(request):
    request.user.auth_token.delete()
    logout(request)
    result = {
        "success": True,
        "message": "User logged out."
    }
    return JsonResponse(result, status=HTTP_200_OK)
