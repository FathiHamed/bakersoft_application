# bakersoft_application
This project is focused on the creation of an application to record time-tracking information of users using Django and Django Rest Framework.

This project include endpoints for authentication, managing projects and the time logs that users have created for them. More precisely, it consists of two apps:
1. The account app where authentication endpoints are located.
2. The time_tracker app where projects and their time logs are managed.

**Using the Application**

- First of all, it is necessary for the user to register in this system and to do this, he can use `register-user` endpoint. It should be noted that in this case, a normal user with limited access will be created. If you want to have all the necessary permissions, you need to create an admin user for yourself using the following command: `python manage.py createsuperuser`.
- In the next step, the user needs to log in to the system to get his own token. `login-user` endpoint has been implemented for this purpose. 
- Finally, if the user wants to manage related projects and create his new logs, he needs to send a request to the relevant endpoints with his own token.

For more information on how this system works, you can see the test suites.
